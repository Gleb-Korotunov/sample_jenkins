pipeline {
    agent any

    environment {
        REPO_URL = 'http://gitlab.com/Gleb-Korotunov/sample_jenkins.git'
        DOCKER_IMAGE = 'php:8.0.3-cli'
    }

    stages {
        stage('Clone Repository') {
            steps {
                git url: "${env.REPO_URL}", branch: 'test_pipeline'
            }
        }

        stage('Run PHP_CodeSniffer') {
            steps {
                script {
                    docker.image("${env.DOCKER_IMAGE}").inside {
                        sh 'curl -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar'
                        sh 'php phpcs.phar --standard=PSR12 /src'
                    }
                }
            }
        }

        stage('Run PHP Mess Detector') {
            steps {
                script {
                    docker.image("${env.DOCKER_IMAGE}").inside {
                        sh 'curl -OL http://static.phpmd.org/php/latest/phpmd.phar'
                        sh 'php phpmd.phar /src text codesize,unusedcode,naming'
                    }
                }
            }
        }
    }

    post {
        always {
            archiveArtifacts artifacts: '**/phpcs.log, **/phpmd.log', allowEmptyArchive: true
        }
        failure {
            mail to: 'korotunov19@gmail.com',
                 subject: "Build Failed in Jenkins: ${currentBuild.fullDisplayName}",
                 body: "Something is wrong with ${env.REPO_URL} - Please go to ${env.BUILD_URL} and fix the build."
        }
    }
}
